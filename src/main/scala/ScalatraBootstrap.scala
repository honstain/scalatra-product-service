import org.bitbucket.honstain.app._
import org.scalatra._
import javax.servlet.ServletContext

class ScalatraBootstrap extends LifeCycle with DatabaseInit {

  override def init(context: ServletContext) {
    context.mount(new ToyProduct(database), "/*")
  }

  override def destroy(context: ServletContext): Unit = {
    super.destroy(context)
    database.close()
  }
}
