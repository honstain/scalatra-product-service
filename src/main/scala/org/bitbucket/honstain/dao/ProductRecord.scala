package org.bitbucket.honstain.dao

import org.slf4j.{Logger, LoggerFactory}
import slick.jdbc.PostgresProfile
import slick.jdbc.PostgresProfile.api._

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

case class ProductRecord(
                          id: Option[Int],
                          sku: String,
                          description: String,
                          lotControl: Boolean,
                          qualityAssuranceHold: Boolean
                        )

class ProductRecords(tag: Tag) extends Table[ProductRecord](tag, "product") {

  def id = column[Int]("id", O.PrimaryKey, O.AutoInc)

  def sku = column[String]("sku", O.Unique)

  def description = column[String]("description")

  def lotControl = column[Boolean]("lot_control")

  def qualityAssuranceHold = column[Boolean]("quality_assurance_hold")

  def * =
    (id.?, sku, description, lotControl, qualityAssuranceHold) <>
      (ProductRecord.tupled, ProductRecord.unapply)
}

object ProductRecordDao extends TableQuery(new ProductRecords(_)) {

  val logger: Logger = LoggerFactory.getLogger(getClass)

  def findAll(db: PostgresProfile.backend.DatabaseDef): Future[Seq[ProductRecord]] = {
    db.run(this.result)
  }

  def findBySku(db: PostgresProfile.backend.DatabaseDef, sku: String): Future[Option[ProductRecord]] = {
    val findAction = this.filter(_.sku === sku)
    db.run(findAction.result.headOption)
  }

  def create(db: PostgresProfile.backend.DatabaseDef,
             sku: String,
             description: String,
             lotControl: Boolean,
             qualityAssuranceHold: Boolean
            ): Future[Option[ProductRecord]] = {

    val insertAndGet = for {
      _ <- TableQuery[ProductRecords] +=
        ProductRecord(Option.empty, sku, description, lotControl, qualityAssuranceHold)

      readResult <- this.filter(x => x.sku === sku).result.headOption
    } yield readResult

    db.run(insertAndGet)
  }
}
