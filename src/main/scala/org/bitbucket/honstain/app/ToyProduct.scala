package org.bitbucket.honstain.app

import org.bitbucket.honstain.dao.{ProductRecord, ProductRecordDao}
import org.scalatra._
import org.slf4j.{Logger, LoggerFactory}

import scala.util.{Failure, Success}
// JSON-related libraries
import org.json4s.{DefaultFormats, Formats}
// JSON handling support from Scalatra
import org.scalatra.json._

import slick.jdbc.PostgresProfile

import scala.concurrent.{Await, Future}
import scala.concurrent.duration.Duration
import scala.concurrent.ExecutionContext.Implicits.global

class ToyProduct(database: PostgresProfile.backend.DatabaseDef) extends ScalatraServlet with JacksonJsonSupport {

  val logger: Logger = LoggerFactory.getLogger(getClass)
  protected implicit val jsonFormats: Formats = DefaultFormats

  val dao = ProductRecordDao

  before() {
    contentType = formats("json")
  }

  def time[R](ident: String)(block: => R): R = {
    val t0 = System.currentTimeMillis()
    val result = block
    val t1 = System.currentTimeMillis()
    logger.debug(s"$ident Time:${t1 - t0}ms")
    result
  }

  get("/") {
    val futureResult = time("findAll") { Await.result(dao.findAll(database), Duration.Inf) }
    logger.debug(s"GET retrieved ${futureResult.size} product records")
    Ok(futureResult.map(x => Product(x.sku, x.description, x.lotControl, x.qualityAssuranceHold)))
  }

  get("/bySku") {
    val futureResult = time("findBySku") { Await.result(dao.findBySku(database, params("sku")), Duration.Inf) }
    logger.debug(s"GET /bySku retrieved ${futureResult.size} product records")
    if (futureResult.nonEmpty) {
      Ok(futureResult.map(x => Product(x.sku, x.description, x.lotControl, x.qualityAssuranceHold)))
    }
    else {
      NotFound("Unable to find product record with that SKU")
    }
  }

  post("/") {
    val newProduct = parsedBody.extract[Product]
    logger.debug(s"Creating product for sku:${newProduct.sku}")

    val future: Future[Option[ProductRecord]] = dao.create(
      database,
      newProduct.sku,
      newProduct.description,
      newProduct.lotControl,
      newProduct.qualityAssuranceHold
    )

    future.onComplete {
      case Success(Some(value)) => logger.debug(s"Created new product record id:${value.id}")
      case Success(None) => logger.error(s"Unable to determine if create succeeded for sku:${newProduct.sku}")
      case Failure(t) => logger.error(t.getMessage)
    }
    val result = Await.result(future, Duration.Inf)
    if (result.isDefined) {
      logger.debug(s"Created new inventory record id:${result.get}")
      val newProd = result.get
      Ok(Product(newProd.sku, newProd.description, newProd.lotControl, newProd.qualityAssuranceHold))
    }
    else {
      InternalServerError
    }
  }
}

case class Product(sku: String, description: String, lotControl: Boolean, qualityAssuranceHold: Boolean)
