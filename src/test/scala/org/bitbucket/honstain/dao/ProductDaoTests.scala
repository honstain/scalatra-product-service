package org.bitbucket.honstain.dao

import org.bitbucket.honstain.PostgresSpec
import org.scalatest.BeforeAndAfter
import org.scalatra.test.scalatest._
import slick.dbio.DBIO
import slick.jdbc.PostgresProfile.api._

import scala.concurrent.Await
import scala.concurrent.duration.Duration


class ProductDaoTests extends ScalatraFunSuite with BeforeAndAfter with PostgresSpec {

  def createProductTable: DBIO[Int] =
    sqlu"""
          CREATE TABLE product
          (
            id bigserial NOT NULL,
            sku text UNIQUE,
            description text,
            lot_control boolean,
            quality_assurance_hold boolean,
            CONSTRAINT pk_product PRIMARY KEY (id)
          );
      """
  def dropProductTable: DBIO[Int] = sqlu"DROP TABLE IF EXISTS product;"

  before {
    Await.result(database.run(createProductTable), Duration.Inf)
  }

  after {
    Await.result(database.run(dropProductTable), Duration.Inf)
  }

  val TEST_SKU_01 = "NewSku-01"
  val TEST_SKU_02 = "NewSku-02"
  val DESC = "Fake description"

  def createProductHelper(sku: String,
                          description: String = DESC,
                          shipsInOwnContainer: Boolean = false,
                          qualityAssuranceHold: Boolean = false): ProductRecord = {
    val create = ProductRecordDao.create(database, sku, description, shipsInOwnContainer, qualityAssuranceHold)
    Await.result(create, Duration.Inf).get
  }

  test("findBySku for nothing found") {
    val future = ProductRecordDao.findBySku(database, "NotFoundSku")
    val result: Option[ProductRecord] = Await.result(future, Duration.Inf)

    result should equal(None)
  }

  test("findBySku for single SKU") {
    createProductHelper(TEST_SKU_01)
    val createdProduct2 = createProductHelper(TEST_SKU_02)

    val future = ProductRecordDao.findBySku(database, TEST_SKU_02)
    val result: Option[ProductRecord] = Await.result(future, Duration.Inf)

    result should equal(Some(createdProduct2))
  }

  test("create") {
    val future = ProductRecordDao.create(database, TEST_SKU_01, DESC, false, false)
    val result: Option[ProductRecord] = Await.result(future, Duration.Inf)

    // I really didn't want to match on the id (since the DB sets it as auto-increment)
    result should equal(Some(ProductRecord(Some(1), TEST_SKU_01, DESC, false, false)))
  }

  test("findAll") {
    createProductHelper(TEST_SKU_01, DESC)
    createProductHelper(TEST_SKU_02, DESC)

    val futureFind = ProductRecordDao.findAll(database)
    val findResult: Seq[ProductRecord] = Await.result(futureFind, Duration.Inf)

    findResult should contain only (
      ProductRecord(Some(1), TEST_SKU_01, DESC, false, false),
      ProductRecord(Some(2), TEST_SKU_02, DESC, false, false),
    )
  }
}