package org.bitbucket.honstain.app

import org.bitbucket.honstain.PostgresSpec
import org.json4s.DefaultFormats
import org.scalatra.test.scalatest._
import org.json4s.jackson.Serialization.write
import org.scalatest.BeforeAndAfter
import slick.dbio.DBIO
import slick.jdbc.PostgresProfile.api._

import scala.concurrent.Await
import scala.concurrent.duration.Duration


class ToyProductTests extends ScalatraFunSuite with BeforeAndAfter with PostgresSpec {

  implicit val formats = DefaultFormats

  addServlet(new ToyProduct(database), "/*")

  def createInventoryTable: DBIO[Int] =
    sqlu"""
        CREATE TABLE product
        (
          id bigserial NOT NULL,
          sku text UNIQUE,
          description text,
          lot_control boolean,
          quality_assurance_hold boolean,
          CONSTRAINT pk_product PRIMARY KEY (id)
        );
      """
  def dropInventoryTable: DBIO[Int] = sqlu"DROP TABLE IF EXISTS product"

  before {
    Await.result(database.run(createInventoryTable), Duration.Inf)
  }

  after {
    Await.result(database.run(dropInventoryTable), Duration.Inf)
  }

  test("GET / on ToyProduct should return status 200") {
    get("/") {
      status should equal (200)
    }
  }

  test("GET / on ToyProduct with no products in DB should return empty list") {
    get("/") {
      body should equal (write(List()))
    }
  }

  test("GET / on ToyProduct should return complete product list") {
    post("/", write(Product("ZL104", "Test Desc", false, false))) {}
    get("/") {
      body should equal (write(List(Product("ZL104", "Test Desc", false, false))))
    }
  }

  test("GET /bySku on ToyProduct should return specific product record") {
    post("/", write(Product("ZL104", "Test Desc", false, false))) {}
    get("/bySku?sku=ZL104") {
      body should equal (write(Product("ZL104", "Test Desc", false, false)))
    }
  }

  test("GET /bySku on ToyProduct for unknown sku should return 404") {
    get("/bySku?sku=ZL104") {
      status should equal (404)
    }
  }

  test("POST / on ToyProduct should return status 200") {
    post("/", write(Product("ZL104", "Test Desc", false, false))) {
      status should equal (200)
      body should equal(write(Product("ZL104", "Test Desc", false, false)))
    }
  }
}