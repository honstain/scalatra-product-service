# ToyProductApp #

This is an experiment Scala service meant to demonstrate a system for modeling supply chain product records. It was created by expanding on the follow blog post http://honstain.com/scalatra-2-6-4-postgresql/

## Build & Run ##

```sh
$ sbt
> jetty:start
> browse
```

If `browse` doesn't launch your browser, manually open [http://localhost:8080/](http://localhost:8080/) in your browser.

The basic REST endpoints supported are:

* GET localhost:8080/ - return all the known products
* GET localhost:8080/bySku?sku=SKU-01 - search for a specific product by SKU, will return 404 if not found
* POST localhost:8080/ - attempt to create a new product record with body:`{"sku":"FOO-01","description":"Test product","lotControl":false,"qualityAssuranceHold":false}`


### Tests
The tests are very slow, because it will attempt to create a new database for each test.
Each database is created with a name representing the test class being run.
NOTE - you will need an environment variable POSTGRES_DATABASE_URL
```sh
$ export POSTGRES_DATABASE_URL="jdbc:postgresql://localhost:5432/toyproduct?user=toyinventory"
$ sbt test
```

## Helpful SQL

```sql
CREATE TABLE product
(
  id bigserial NOT NULL,
  sku text UNIQUE,
  description text,
  lot_control boolean,
  quality_assurance_hold boolean,
  CONSTRAINT pk_product PRIMARY KEY (id)
);

SELECT * FROM product;

INSERT INTO product (sku, description, lot_control, quality_assurance_hold)
VALUES ('SKU-01', 'Test Sku 01', false, false),
       ('SKU-02', 'Test Sku 01', false, false),
       ('SKU-03', 'Test Sku 01', false, false)
;
```

